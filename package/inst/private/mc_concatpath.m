function str = mc_concatpath(varargin)
  %MC_CONCATPATH  Concatenate file parts with correct file separator.
  %		STR = MC_CONCATPATH(STR1, STR2, ...) concatenates file/path parts with the
  %		system file separator.
  %
  %		Example:
  %		drive = 'C:';
  %		fileName = 'test.txt';
  %		fullFileName = mc_concatpath(drive, 'My documents', fileName);
  %	
  %		Markus Buehren
  %		Last modified 05.04.2009
  %             Stanislav Mašláň
  %		Last modified 27.11.2015
  %             
  %
  %		See also FULLFILE, FILESEP, MC_CHOMPSEP.
  
  str = '';
  for n=1:nargin
  	curStr = varargin{n};
  	str = fullfile(str, mc_chompsep(curStr));
  end
  
  if ispc
    str = strrep(str, '/', '\');
  else
    str = strrep(str, '\', '/');
  end  

endfunction
