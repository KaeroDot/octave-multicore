%             Stanislav Mašláň
%		Last modified 27.11.2015
function [fHandles,is_ok] = mc_rebuild_function_handles(fStrs)
  
  is_ok = 1;
    
  try
    % try to rebuild function handles
    if isa(fStrs,'char')
      % return function handle as it is
      fHandles = str2func(fStrs);
              
    elseif iscell(fStrs) 
      if all(size(fStrs) == [1 1])
        % return function handle
        fHandles = str2func(fStrs{1});      
        
      else
        if numel(fStrs) == 1
          % return function handle
          fHandles = str2func(fStrs{1});
                      
        else
          % return function handle cell        
          fHandles = cell(size(fStrs));
          for k = 1:numel(fStrs)
            fHandles{k} = str2func(fStrs{k}); 
          endfor
          
        end
      end
    else
      error('Input type unknown.');
    end  
  catch
    % failed
    is_ok = 0;
  end  
    
endfunction
