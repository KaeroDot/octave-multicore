%             Stanislav Mašláň
%		Last modified 27.11.2015

function [] = mc_console_progress_bar(files_N,files_proc_m,files_proc_s)

  persistent t_start=0;
  
  % initial time
  if(~t_start)
    t_start = time();
  endif
  
  % current time
  t_cur = time();
  
  % total processed files
  files_proc = files_proc_m + files_proc_s;
    
  % update?
  if files_N && files_proc    
    
    % estimated finish time
    t_end = t_start + files_N*(t_cur-t_start)/files_proc;
    
    % remaining        
    t_remain = t_end - t_cur;
    
    % print status
    t_end_str = strftime('%k:%M:%S %Y-%m-%d',localtime(t_end));            
    fprintf('MC progress ... %0.1f%%, END = %s (T = -%s)                   \r',100*files_proc/files_N,t_end_str,mc_seconds_to_str(t_remain));
  else
    fprintf('MC progress ... %0.1f%%\r',0);
  end	  
  
endfunction
