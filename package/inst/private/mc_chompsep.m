function str = mc_chompsep(str)
  %MC_CHOMPSEP  Remove file separator at end of string.
  %		STR = MC_CHOMPSEP(STR) returns the string STR with the file separator at
  %		the end of the string removed (if existing). 
  %
  %		Example:
  %		str1 = mc_chompseq('/usr/local/');
  %		str2 = mc_chompseq('C:\Program Files\');
  %
  %		Markus Buehren
  %		Last modified 05.04.2009
  %		Stanislav Mašláň
  %		Last modified 27.11.2015
  %
  %		See also MC_CONCATPATH.
  
  if ~isempty(str) && str(end) == filesep
    str(end) = '';
  end
endfunction

