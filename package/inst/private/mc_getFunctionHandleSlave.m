%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Stanislav Mašláň
%		Last modified 27.11.2015
function fHandles = mc_getFunctionHandleSlave(functionHandleCell, index)

  if isa(functionHandleCell,'function_handle')
    % return function handle as it is
    fHandles = functionHandleCell;
  elseif iscell(functionHandleCell)
    if isa(functionHandleCell{index},'function_handle')
      % return function handle
      fHandles = functionHandleCell{index};
    else
      error('Input type unknown.');
    endif
  else
    error('Input type unknown.');
  end  
  
endfunction
