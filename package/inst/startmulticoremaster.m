## Copyright (C) Markus Buehren 2011, Stanislav Mašláň 2015, Martin Šíra 2018
##

## -*- texinfo -*-
## @deftypefn {Function File} @var{resultCell} = startmulticoremaster(@var{functionHandle}, @var{parameterCell})
## @deftypefnx {Function File} @var{resultCell} = startmulticoremaster(@var{functionHandleCell}, @var{parameterCell})
## @deftypefnx {Function File} @var{resultCell} = startmulticoremaster(..., @var{settings})
## Starts a multi-core processing master process. The function specified
## by the given function handle is evaluated with the parameters saved in
## each cell of @var{parameterCell}. Each cell may include parameters in any
## form or another cell array which is expanded to an argument list using
## the @{:@} notation to pass multiple input arguments (this can be disabled using
## settings CellExpansion). The outputs of the function are returned in cell array
## @var{resultCell} of the same size as @var{parameterCell}. Only the first output argument of the
## function is returned. If you need to get multiple outputs, write a small adapter that puts the
## outputs of your function into a single cell array.
##
## If cell of function handles @var{functionHandleCell} is supplied as first 
## argument, one can evaluate different functions. 
##
## To make use of multiple cores/machines, function STARTMULTICOREMASTER
## saves files with the function handle and the parameters to a temporary
## directory (default: ./multicorefiles). These files are loaded by
## function STARTMULTICORESLAVE running in other Matlab processes which
## have access to the temporary directory. The slave processes evaluate
## the given function with the saved parameters and save the result in
## another file. The results are later collected by the master process.
##
## Note that you can make use of multiple cores on a single machine or on
## different machines with a commonly accessible directory/network share
## or a combination of both.
##
## The additional input structure @var{settings} may contain any of the
## following fields:
## @table @code
## @item multicoreDir
##   Directory for temporary files (directory ./multicorefiles is used if field missing or empty)
## @item nrOfEvalsAtOnce
##   Number of function evaluations gathered to a single job (default value 1).
## @item maxJobFiles
##   Limits maximum job files count. Has higher priority than parameter 
##   nrOfEvalsAtOnce. Use 0 if you don't want to use this (default).
## @item masterIsWorker
##   If true (default), master process acts as worker and coordinator, if false the
##   master acts only as coordinator.
## @item moreTalk
##   Value 0 displays nothing. Value 1 displays brief infos. Value 2 
##   displays progress info.
## @item paths
##   Cell of strings with user defined paths. These paths will be loaded
##   automatically by the slaves. Usually one wants to put here paths 
##   to the directories containing scripts outside the working directory.
## @item CellExpansion
##   If set to 0 and parameterCell is cell of cells, it will not be expanded
##   to argument list using @{:@} notation. However the function can have only
##   one input argument. Useful for passing cells as input arguments. The same
##   have to be set for slaves.
## @item timeLimit
##   If set to value greater than 0, the master process will wait since last
##   collected result maximally timeLimit seconds. Because the waiting of
##   the mean waiting loop is not linear, the timing is not exact. Also can
##   be affected if masterIsWorker.
## @end table
## 
## Example:
## Start multiple instances of octave. In each run following:
## @example
## startmulticoreslave
## @end example
## Start another instance of octave. Run these commands:
## @example
## paramcell = num2cell(repmat(1,10,1));
## opt.moreTalk = 2;
## startmulticoremaster(@@mc_testfun, paramcell, opt)
## @end example
## @seealso{startmulticoreslave, runmulticore}
## @end deftypefn

function resultCell = startmulticoremaster(functionHandleCell,parameterCell,settings, debugMode=0)
 
  % check for debugMode and assign 1 or 0 value
  if debugMode
          debugMode = 1;
  else
          debugMode = 0;
  endif
  showWarnings = 0;
  
  % parameters
  startPauseTime = 0.1;
  maxPauseTime   = 2;
  
  % default settings/parameters
  settingsDefault.multicoreDir        = '';
  settingsDefault.nrOfEvalsAtOnce     = 1;
  settingsDefault.maxJobFiles         = 0;
  settingsDefault.maxEvalTimeSingle   = 60;
  settingsDefault.masterIsWorker      = 1;
  settingsDefault.moreTalk            = 0;
  settingsDefault.paths               = {};
  settingsDefault.CellExpansion       = 1;
  settingsDefault.postProcessHandle   = '';  
  settingsDefault.postProcessUserData = {};
  settingsDefault.timeLimit           = 0;
    
  if debugMode
    fprintf('*********** Start of function %s **********\n',mfilename);
    startTime    = time();
    showWarnings = 1;
    setTime      = 0;
    removeTime   = 0;
    warning('on', 'multicore:dir-stat');
    warning('on', 'multicore:dir-lstat');
  else
    % disable warnings which happens when job files disappears during reading content of directory:
    warning('off', 'multicore:dir-stat');
    warning('off', 'multicore:dir-lstat');
  end
  
  % check inputs
  error(nargchk(2,4,nargin,'struct'));
  
  % check function handle cell
  if ~isa(functionHandleCell,'function_handle')
    if ~iscell(functionHandleCell)
      error('First input argument must be a function handle or a cell array of function handles.');
    elseif ~all(size(functionHandleCell) == [1 1]) && ~all(size(functionHandleCell) == size(parameterCell))
      error(['Input cell array functionHandleCell must be of size 1x1 or the same size as the parameterCell.']);
    end
  end
  
  % check parameter cell
  if ~iscell(parameterCell)
    error('Second input argument must be a cell array.');
  end
  
  % get settings
  if ~exist('settings','var')
    % use default settings
    settings = settingsDefault;
  else
    settings = mc_combineSettings(settings, settingsDefault);
  end
  
  % master can evaluate jobs?
  masterIsWorker = settings.masterIsWorker;
    
  % more talk {0 - nothing, 1 - only brief status, 2 - print progress info}
  moreTalk = settings.moreTalk;

  % no cell of cells expanstion - 0 no expansion, 1 - normal expansion, evaluated function can have
  % multiple input arguments
  CellExpansion = settings.CellExpansion;
  
  % check number of evaluations at once
  nrOfEvals = numel(parameterCell);
  nrOfEvalsAtOnce = settings.nrOfEvalsAtOnce;
  if nrOfEvalsAtOnce > nrOfEvals
    nrOfEvalsAtOnce = nrOfEvals;
  elseif nrOfEvalsAtOnce < 1
    error('Parameter nrOfEvalsAtOnce must be greater or equal one.');
  end
  nrOfEvalsAtOnce = round(nrOfEvalsAtOnce);
    
  % maximum job files count specified? 
  if settings.maxJobFiles && ceil(nrOfEvals/nrOfEvalsAtOnce) > settings.maxJobFiles
    % yaha, setup 'nrOfEvalsAtOnce' to limit job files count to desired value      
    nrOfEvalsAtOnce = ceil(nrOfEvals/settings.maxJobFiles);    
  endif
   
  % time limit for waiting since last result?
  timeLimit = settings.timeLimit;
    
  % create slave file directory if not existing
  if moreTalk, fprintf('Creating job share folder ... '); endif
  multicoreDir = settings.multicoreDir;
  if isempty(multicoreDir)
  	multicoreDir = fullfile('.', 'multicorefiles');
  end
  if ~exist(multicoreDir, 'dir')
  	try
  		mkdir(multicoreDir);
  	catch
  		error('Unable to create slave file directory %s.', multicoreDir);
  	end
  end
  if moreTalk, fprintf('done\n'); endif
      
  % check maxEvalTimeSingle (### not used?)
  maxEvalTimeSingle = settings.maxEvalTimeSingle;
  if maxEvalTimeSingle < 0
    error('Parameter maxEvalTimeSingle must be greater or equal zero.');
  end
  
  % compute the maximum waiting time for a complete job (### not used?)
  maxMasterWaitTime = maxEvalTimeSingle * nrOfEvalsAtOnce;
  
  % compute number of files/jobs
  nrOfFiles = ceil(nrOfEvals/nrOfEvalsAtOnce); % ###note: may be rounding error +1???
  if debugMode
    fprintf('nrOfFiles = %d\n', nrOfFiles);
  end
  
  % Initialize structure for postprocessing function (### not used)
  if ~isempty(settings.postProcessHandle)
    postProcStruct.state               = 'initialization';
    postProcStruct.nrOfFiles           = nrOfFiles;
    postProcStruct.functionHandleCell  = functionHandleCell;
    postProcStruct.parameterCell       = parameterCell;
    postProcStruct.userData            = settings.postProcessUserData;
    feval(settings.postProcessHandle, postProcStruct);
  end
  
  % remove all existing temporary multicore files
  if moreTalk, fprintf('Removing existing jobs ... '); endif
  existingMulticoreFiles = [...
    mc_findfiles(multicoreDir, 'parameters_*.mat', 'nonrecursive'), ...
    mc_findfiles(multicoreDir, 'parameters_*.tmp', 'nonrecursive'), ...
    mc_findfiles(multicoreDir, 'result_*.mat',     'nonrecursive'), ...
    mc_findfiles(multicoreDir, 'result_*.tmp', 'nonrecursive')];
  cellfun(@unlink,existingMulticoreFiles);
  if moreTalk, fprintf('done\n'); endif
  
  % build parameter file name (including the date is important because slave
  % processes might still be working with old parameters)
  dateStr = sprintf('%04d%02d%02d%02d%02d%02d', round(clock));
  parameterFileNameTemplate = fullfile(multicoreDir, sprintf('parameters_%s_XX.mat', dateStr));
  parameterFileNameTemplate_tmp = fullfile(multicoreDir, sprintf('parameters_%s_XX.tmp', dateStr));
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % generate parameter files %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%  
  
  % save parameter files with all parameter sets
  curFileNr = 0;
  for k = 1:nrOfEvalsAtOnce:nrOfEvals
    
    % current file index:
    curFileNr++;
    
    % generate job batch file name:
    parameterFileName =     strrep(parameterFileNameTemplate, 'XX', sprintf('%04d', curFileNr));
    parameterFileName_tmp = strrep(parameterFileNameTemplate_tmp, 'XX', sprintf('%04d', curFileNr)); % ###note: was not _tmp! so the rename() had no effect
    % jobs for this batch file:
    parIndex = k:min(k + nrOfEvalsAtOnce - 1,nrOfEvals);
    % rebuild function handles for the batch:
    [functionHandles,functionHandlesStr] = mc_getFunctionHandles(functionHandleCell, parIndex); %#ok
    % get jobs list:
    parameters = parameterCell(parIndex); %#ok
    
    % list of user defined paths that will be loaded automatically by the slaves
    % note: this does most likely sense only on single computer...
    userPaths = settings.paths;
    
    % save temp job batch file:
    try
      save('-binary',parameterFileName_tmp,'parameters','functionHandlesStr','functionHandles','userPaths','CellExpansion'); %% file access %%
      if debugMode
        fprintf('Parameter file nr %d generated.\n', curFileNr);
      end
    catch
      if showWarnings
        disp(sprintf('Warning: Unable to save file ''%s''.', parameterFileName_tmp));
        %mc_displayerrorstruct;
      end
    end
    
    % activate job file (rename to final name - should be atomic operation):
    [err,msg] = rename(parameterFileName_tmp,parameterFileName);    
  
    if moreTalk, fprintf('Generating new job files ... %d of %d\r',curFileNr,nrOfFiles); endif
  end
  if moreTalk, fprintf('\n'); endif
  
  
  % initialize results list: 
  resultCell = cell(size(parameterCell));
    
  parameterFileFoundTime  = NaN;
  parameterFileRegCounter = 0;
  nrOfFilesMaster = 0;
  nrOfFilesSlaves = 0;
   
  % Call "clear functions" to ensure that the latest file versions are used,
  % no older versions in Matlab's memory.
  % ###note: this is not needed, in fact it is quite harmful because it erases cache of calling function
  %          which results into loss of nested functions...
  %clear functions;
  % add paths defined by user before execution:
  % ###todo: it would be nice to remove them at the end, but only if they were not already there!
  for k = 1:numel(settings.paths)
    addpath(settings.paths{k});      
  endfor
  
  
  
  % progress bar
  if moreTalk>1, mc_console_progress_bar(nrOfFiles,nrOfFilesMaster,nrOfFilesSlaves); endif
    
  % no results found yet
  resultsCount = 0;
  
  % no parameter files found yet (for master worker)
  parameterFileList = {};
  parameterCount = 0;
  
  firstRun = true;
  curPauseTime = startPauseTime;
  TimeOfLastResult = time;
  while 1 % this while-loop will be left if all work is done (hopefully)    
  % this while loop breaks at two places:
  %     1, if all work is done
  %     2, if timeLimit since last obtained result is reached
     
    if (nrOfFilesMaster + nrOfFilesSlaves) >= nrOfFiles
      % all work is done
      if debugMode
        disp('********************************');
        fprintf('All work is done.\n');
      end
      break;
    end              
          
    if ~resultsCount
      % --- no files in temp results list - search again:
    
      % build result search mask
      [mc_dir,pfile_name,pfile_ext] = fileparts(parameterFileNameTemplate);
      parameterFileName = strrep([pfile_name pfile_ext],'XX','*');
      resultFileName    = strrep(parameterFileName,'parameters','result');
    
      % search all available results
      resultFileList = mc_findfiles(mc_dir,resultFileName,'nonrecursive');      
      
      % total files count
      resultsCount = numel(resultFileList);
      
      % current position in searched result files buffer
      resultId = 0;
            
    endif   
        
    % some result found?
    resultFileName = '';
    if resultsCount
      % yaha, parse file name
      
      % get first file from the results list
      resultFileName = resultFileList{++resultId};
      
      % update remaining files
      resultsCount--;     
              
      % stripe result file path
      [mc_dir,rfile_name] = fileparts(resultFileName);
      
      % extract result id
      [a,b,c,d,stdid] = regexpi(rfile_name,'result_[\d]+_([\d]+)[^\\\/]*$');
      curFileNr = str2num(stdid{1}{1});
      
      % build parameter and working files names for the result file
      %parameterFileName = strrep(resultFileName,rfile_name,'parameters');
      %workingFileName = strrep(resultFileName,rfile_name,'working');
      
      % results position in the result buffer
      parIndex = ((curFileNr-1)*nrOfEvalsAtOnce+1):min(curFileNr*nrOfEvalsAtOnce,nrOfEvals);                      
   
    endif                  
      
    % Check if the result is available (the semaphore file of the
    % parameter file is used for the following file accesses of the
    % result file)
    if ~isempty(resultFileName) && mc_existfile(resultFileName)
      % yaha - exist:
         
      % try to load it:
      [result, resultLoaded] = mc_loadResultFile(resultFileName, showWarnings);
      
      if resultLoaded
        % update files counter
        nrOfFilesSlaves++;
      else
        % file exist, but loading failed??? This should not happen...
        
        fprintf('Shiiieeeeet! Result file name ''%s'' is non-empty, exist but loading failed?',resultFileName);
        resultsCount = 0; % this should cause new results files search because something went wrong broah..
                
      endif
      
      if resultLoaded && debugMode
        fprintf('Result file nr %d loaded.\n', curFileNr);
      end
    elseif ~isempty(resultFileName)
        fprintf('Shiiieeeeet! Result file name ''%s'' is non-empty but does not exist?',resultFileName);
        resultsCount = 0; % this should cause new results files search because something went wrong broah..
        
    else
      resultLoaded = false;
      if debugMode
        fprintf('Result file nr %d was not found.\n', curFileNr);
      end
    end
    
    %% Try process some job by the master? 
    if masterIsWorker && ~resultLoaded && ~resultsCount
      % --- yes we can - nothing to do for master:
            
      %% search for job files again?
      if ~parameterCount
      
        % yaha, search parameter files
        parameterFileList = mc_findfiles(multicoreDir, 'parameters_*.mat', 'nonrecursive');
              
        % reset file list
        parameterCount = numel(parameterFileList);
        parameterId = 0;
        
        % randomize list
        if parameterCount
          rnd_ids = randperm(parameterCount);
          parameterFileList(:) = parameterFileList(rnd_ids);  
        endif
              
      endif
                     
      % get file from list
      parameterFileName = '';
      if parameterCount
        parameterFileName = parameterFileList{++parameterId};
        parameterCount--;
      endif
      
             
      if ~isempty(parameterFileName)      
        
        % try to get exclusive access to the parameter file (atomic operation):
        [parameterFileName_tmp,p_tmp_ok] = mc_try_get_params_file(parameterFileName);
            
        % try to load the file:
        loadSuccessful = 0;
        if p_tmp_ok && mc_existfile(parameterFileName_tmp)          
          lastwarn('');
          lasterror('reset');
          clear functionHandlesStr functionHandles parameters;
          loadSuccessful = 1;
          try
            load('-binary',parameterFileName_tmp,'parameters','functionHandlesStr','functionHandles'); %% file access %%
          catch
            
            % something gone wrong
            if exist('parameters','var') && exist('functionHandlesStr','var') && ~exist('functionHandles','var')
              % missing function handles: the absolute function path in the function handle variable is likely not valid
              %                           try to find the function(s) in search path and rebuild function handle(s)
              [functionHandles,funs_ok] = mc_rebuild_function_handles(functionHandlesStr);
              
              % check success
              if ~funs_ok
                loadSuccessful = 0;
                fprintf('Warning: Unable to rebuild function handles in new location.\n');
                lastMsg = lastwarn;
                if ~isempty(lastMsg)
                  fprintf('Warning message issued when trying to load:\n%s\n', lastMsg);
                end              
                %mc_displayerrorstruct;              
              endif
              
            else
            
              loadSuccessful = 0;
              if showWarnings
                fprintf('Warning: Unable to load parameter file %s.\n', parameterFileName_tmp);
                lastMsg = lastwarn;
                if ~isempty(lastMsg)
                  fprintf('Warning message issued when trying to load:\n%s\n', lastMsg);
                end
                %mc_displayerrorstruct;
              end
            endif
          end
        endif
        
        % check if variables to load are existing
        if loadSuccessful && (~exist('functionHandles', 'var') || ~exist('parameters', 'var'))
          % failed:
          loadSuccessful = 0;
          if showWarnings
            disp(sprintf(['Warning: Either variable ''%s'' or ''%s''', ...
              'or ''%s'' not existing after loading file %s.'], ...
              'functionHandles', 'parameters', parameterFileName));
          end
        end
        
        % remove temp parameter file    
        [err,msg] = unlink(parameterFileName_tmp);      
        if err
          % If deletion is not successful it can happen that other slaves or
          % the master also use these parameters. To avoid this, ignore the
          % loaded parameters
          loadSuccessful = 0;
          if debugMode
            fprintf('Problems deleting parameter file nr %s. It will be ignored.\n', parameterFileName_tmp);
          end
        end
        
        % try to process
        if loadSuccessful
        
          % break line if status bar is used
          if moreTalk>1 && masterIsWorker, fprintf('\n'); endif
        
          % process jobs
          result = cell(size(parameters)); %#ok
          for k = 1:numel(parameters)
            if iscell(parameters{k}) && CellExpansion
              result{k} = feval(mc_getFunctionHandleSlave(functionHandles, k), parameters{k}{:}); %#ok
            else
              result{k} = feval(mc_getFunctionHandleSlave(functionHandles, k), parameters{k}); %#ok
            end
          end
          
          % stripe result file path
          [mc_dir,pfile_name] = fileparts(parameterFileName);
          
          % extract result id
          [a,b,c,d,stdid] = regexpi(pfile_name,'parameters_[\d]+_([\d]+)[^\\\/]*$');
          curFileNr = str2num(stdid{1}{1});                
          
          % results position in the result buffer
          parIndex = ((curFileNr-1)*nrOfEvalsAtOnce+1):min(curFileNr*nrOfEvalsAtOnce,nrOfEvals);
          
          % result loaded
          resultLoaded = 1;
          
          % update files counter
          nrOfFilesMaster++;      
          
        endif
        
        % remove variables before next run
        clear functionHandleStr functionHandle parameters;        
        
      endif
          
    endif
        
    
    
    if resultLoaded            

      % add result(s) to the results list
      resultCell(parIndex) = result;                
        
      % Run postprocessing function (### not implemented)
      if ~isempty(settings.postProcessHandle)
        postProcStruct.state               = 'after loading result'; % no copy & paste here!!
        postProcStruct.nrOfFilesMaster     = nrOfFilesMaster;
        postProcStruct.nrOfFilesSlaves     = nrOfFilesSlaves;
        postProcStruct.resultCell          = resultCell;
        postProcStruct.parIndex            = parIndex;
        feval(settings.postProcessHandle, postProcStruct);
      end

      % Update waitbar
      if moreTalk>1, mc_console_progress_bar(nrOfFiles,nrOfFilesMaster,nrOfFilesSlaves); endif

      % Reset variables
      curPauseTime = startPauseTime;
      % update time of last result:
      TimeOfLastResult = time;

    end % if resultLoaded
    
    % wait?
    if ~masterIsWorker && ~resultsCount
      % If the master is only coordinator, wait some time before
      % checking again
      if debugMode
        fprintf('Coordinator is waiting %.2f seconds\n', curPauseTime);
      end
      pause(curPauseTime);
      curPauseTime = min(maxPauseTime, curPauseTime + startPauseTime);
    else
      % reset pause time
      curPauseTime = startPauseTime;  
    end

    % get time since last obtained result:
    secFromLastResult = (time - TimeOfLastResult);
    % check if waiting too long since last result:
    if  timeLimit > 0 && secFromLastResult > timeLimit
        disp('********************************');
        fprintf('!!! Time limit reached. No result in last %d seconds (%.3f hours). Quitting.', secFromLastResult, secFromLastResult/3600);
        break
    endif
    
    firstRun = false;
  end % while 1
  
  if moreTalk>1, fprintf('\n'); endif
  
  
  if debugMode
    fprintf('\nSummary:\n--------\n');
    fprintf('%2d jobs at all\n',           nrOfFiles);
    fprintf('%2d jobs done by master\n',   nrOfFilesMaster);
    fprintf('%2d jobs done by slave(s)\n', nrOfFilesSlaves);
    
    overallTime = time() - startTime;
    fprintf('Processing took %.1f seconds.\n', overallTime);    
    fprintf('\n*********** End of function %s **********\n', mfilename);
  end

endfunction % function startmulticoremaster

