# Parallel Processing on Multiple Cores and Computers

An GNU Octave package providing functions for parallel processing by means of multiple Octave
instances. Job distribution via shared folder.

This is total overhaul of the multicore package by Markus Buehren, which was modified into GNU
Octave package by Chuong Nguyen.

Advantages when compared to `parcellfun`:
- Parallel calculation can run on multiple cores of single or multiple computers.
- Slaves can wait for next jobs thus can be used for multiple calculations.

Disadvantages when compared to `parcellfun`:
- Significantly higher overhead.
- Requires shared directory for storing temporary files.

## Description

If you have multiple function calls that are independent of each other, and you can reformulate your
code as

    resultCell = cell(size(parameterCell));
    for k=1:numel(parameterCell)
            resultCell{k} = myfun(parameterCell{k});
    end
  
then, replacing the loop by

    resultCell = startmulticoremaster(@myfun, parameterCell);
  
allows you to evaluate your loop in parallel in several processes. All you need to do in the other
GNU Octave processes is to run

    startmulticoreslave;

These `slaves` can be running on the same computer or on some other. The only requirement is to have
a common temporary folder, e.g. on a network drive.

Please refer to the help comments in file `startmulticoremaster.m` for possible input arguments and
more details about the parallelization works.

No special toolboxes are used, no compilation of mex-files is necessary, everything is programmed in
plain m files. If one of your slave processes dies - don't care, the master process will go on
working on the given task.

Please consider that the communication between the processes, which is done by using the file
system, causes some overhead. Thus, you will only notice an improvement in speed if your function
calls need considerable time, let say some seconds. If you have a huge number of function
evaluations to be executed, where every function evaluation only needs a fraction of a second, you
can still use this package. However you can maybe get better results using e.g. `parcellfun`
function.

To simplify parallel calculations a script `runmulticore` can be used. It automatically does
required operations to start slaves. It is also easy to switch from using `multicore` to
`parcellfun` or even `cellfun`. This is useful for debugging.

## Examples
### Example 1
Use of `runmulticore`. Compare calculation times for different methods. Note overhead when using
multicore.

    paramcell = num2cell(repmat(1,10,1));
    tic; res=runmulticore('cellfun', @mc_testfun, paramcell); toc
    tic; res=runmulticore('parcellfun', @mc_testfun, paramcell); toc
    tic; res=runmulticore('multicore', @mc_testfun, paramcell); toc

### Example 2
More complex use of `runmulticore`. Take a look into directory './multicore' during calculation.

    paramcell = num2cell(repmat(1,10,1));
    opt.master_is_worker=false;
    res=runmulticore('multicore', @mc_testfun, paramcell, 2, '', 2, opt);

### Example 3
Direct use of `startmulticoremaster` and `startmulticoreslave`.
Start multiple instances of octave. In each run following:

    startmulticoreslave

Start another instance of octave. Run these commands:

    paramcell = num2cell(repmat(1,10,1));
    opt.moreTalk = 2;
    startmulticoremaster(@mc_testfun, paramcell, opt)
